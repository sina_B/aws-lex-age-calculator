import json
import datetime

def lambda_handler(event, context):
    
    name = event['currentIntent']['slots']['Name']
    birth = int(event['currentIntent']['slots']['YearBirth'])
    current = datetime.datetime.now().year
    
    if birth > current:
        return {
            "dialogAction": {
                "type": "ElicitSlot",
                "message": {
                    "contentType": "PlainText",
                    "content": str(birth) + " is not a valid year. " + name + ", in what year were you born?"
                },
                "intentName": "CalcularEdad",
                "slots": {
                    "Name": name,
                    "YearBirth": str(birth)
                },
                "slotToElicit": "YearBirth"
            }
        }
    else:
        edad = current - birth
        return {
            "dialogAction": {
                "type": "Close",
                "fulfillmentState": "Fulfilled",
                "message": {
                    "contentType": "PlainText",
                    "content": name + ", you are " + str(edad) + " years old."
                }
            }
        }
        
